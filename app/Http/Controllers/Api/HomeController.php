<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            'body' =>'hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiihaaaaaaaaaaa',
            'datetime' => Carbon::now(),
        ]);
    }
}
