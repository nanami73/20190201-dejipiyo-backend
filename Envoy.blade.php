@servers(['ec2' => 'ec2-user@18.176.34.80'])

@setup
    $appDir = '/app'
@endsetup

@story('deploy', ['on' => 'ec2'])
    task-setup
    task-deploy
@endstory

@task('task-setup')
    cd {{ $appDir }}
    git pull origin master
    composer install --no-dev
@endtask

@task('task-deploy')
    cd {{ $appDir }}
    php artisan down
    echo "DB使ってたらここで migrate する"
    php artisan up
@endtask
